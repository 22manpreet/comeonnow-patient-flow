package com.comeonnow.patient.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R
import kotlinx.android.synthetic.main.activity_sign_in.imgShowHidePwdIV
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.editFirstNameET)
    lateinit var editFirstNameET: EditText

    @BindView(R.id.editLastNameET)
    lateinit var editLastNameET: EditText

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    @BindView(R.id.editConfirmPasswordET)
    lateinit var editConfirmPasswordET: EditText

    // - - Initialize Objects
    private var showHide: Int = 1
    private var showHideConfirm: Int = 1
    private var checkUncheck: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        ButterKnife.bind(this)
        setEditTextFocused(editConfirmPasswordET)
    }

    @OnClick(
        R.id.txtLoginTV,
        R.id.imgShowHidePwdIV,
        R.id.imgShowHideConfirmPwdIV,
        R.id.agreeRL,
        R.id.btnSignUpTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtLoginTV -> performLoginClick()
            R.id.imgShowHidePwdIV -> performShowHideClick()
            R.id.imgShowHideConfirmPwdIV -> performShowHideConfirmClick()
            R.id.agreeRL -> performAgreeClick()
            R.id.btnSignUpTV -> performSignUpClick()
        }
    }

    private fun performSignUpClick() {
        if (isValidate()) {
            startActivity(Intent(mActivity, SignInActivity::class.java))
            finish()
        }
    }

    private fun performLoginClick() {
        startActivity(Intent(mActivity, SignInActivity::class.java))
        finish()
    }

    // - - Perform I Agree functionality
    private fun performAgreeClick() {
        if (checkUncheck == 0) {
            imgAgreeIV.setImageResource(R.drawable.ic_check)
            checkUncheck++
        } else {
            imgAgreeIV.setImageResource(R.drawable.ic_uncheck)
            checkUncheck--
        }
    }

    // - - Show or Hide Password
    private fun performShowHideClick() {
        if (showHide == 0) {
            imgShowHidePwdIV.setImageResource(R.drawable.ic_hide_pwd)
            editPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
            editPasswordET.setSelection(editPasswordET.text.length)
            showHide++
        } else {
            imgShowHidePwdIV.setImageResource(R.drawable.ic_show_pwd)
            editPasswordET.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editPasswordET.setSelection(editPasswordET.text.length)
            showHide--
        }

    }

    // - - Show or Hide Confirm Password
    private fun performShowHideConfirmClick() {
        if (showHideConfirm == 0) {
            imgShowHideConfirmPwdIV.setImageResource(R.drawable.ic_hide_pwd)
            editConfirmPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
            editConfirmPasswordET.setSelection(editConfirmPasswordET.text.length)
            showHideConfirm++
        } else {
            imgShowHideConfirmPwdIV.setImageResource(R.drawable.ic_show_pwd)
            editConfirmPasswordET.transformationMethod =
                HideReturnsTransformationMethod.getInstance()
            editConfirmPasswordET.setSelection(editConfirmPasswordET.text.length)
            showHideConfirm--
        }
    }

    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editFirstNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_first_name))
                flag = false
            }
            editLastNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_last_name))
                flag = false
            }
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
            editPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            editConfirmPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_retype_password))
                flag = false
            }
            editPasswordET.text.toString().trim() != editConfirmPasswordET.text.toString()
                .trim() -> {
                showAlertDialog(mActivity, getString(R.string.the_pwd_re_typed))
                flag = false
            }
            checkUncheck == 0 -> {
                showAlertDialog(mActivity, getString(R.string.please_agree))
                flag = false
            }
        }
        return flag
    }
}