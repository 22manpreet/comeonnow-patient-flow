package com.comeonnow.patient.app.ui.activity

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.util.*

class WebViewActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.txtHeadingTV)
    lateinit var txtHeadingTV: TextView

    @BindView(R.id.linkWV)
    lateinit var linkWV: WebView

    // - - Initialize Objects
    var linkType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        ButterKnife.bind(this)
        getIntentData()
    }

    @OnClick(R.id.backRL)
    fun onClick(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    // - - Get Intent Data from Another Activity
    private fun getIntentData() {
        if (intent != null) {
            linkType = intent.getStringExtra(LINK_TYPE).toString()
            when (linkType) {
                LINK_ABOUT -> {
                    txtHeadingTV.text = getString(R.string.about_us)
                    setUpWebView(ABOUT_WEB_LINK)
                }
                LINK_TERMS -> {
                    txtHeadingTV.text = getString(R.string.terms_and_conditions)
                    setUpWebView(TERMS_WEB_LINK)
                }
                LINK_PP -> {
                    txtHeadingTV.text = getString(R.string.privacy_policy)
                    setUpWebView(PP_WEB_LINK)
                }
            }
        }
    }

    // - - Opens WebView in Application
    private fun setUpWebView(mUrl: String) {
        // this will set up webView client
        linkWV.webViewClient = WebViewClient()
        // this will load the url of the website
        linkWV.loadUrl(mUrl)
        // this will enable the javascript settings
        linkWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        linkWV.settings.setSupportZoom(true)
    }

}