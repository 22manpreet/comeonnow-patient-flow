package com.comeonnow.patient.app.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.adapter.HomeAdapter
import com.comeonnow.patient.app.ui.activity.AddPatientActivity
import com.facebook.shimmer.ShimmerFrameLayout

class HomeFragment : BaseFragment() {

    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.addPatientLL)
    lateinit var addPatientLL: LinearLayout

    @BindView(R.id.homeListRV)
    lateinit var homeListRV: RecyclerView

    @BindView(R.id.txtNoDataFoundTV)
    lateinit var txtNoDataFoundTV: TextView

    @BindView(R.id.shimmerLayout)
    lateinit var shimmerLayout: ShimmerFrameLayout

    // - - Initialize Objects
    var mHomeAdapter: HomeAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        setAdapter()
        return view
    }

    @OnClick(
        R.id.addPatientLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.addPatientLL -> performAddPatientClick()
        }
    }

    private fun performAddPatientClick() {
        val intent = Intent(activity, AddPatientActivity::class.java)
        startActivity(intent)
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        homeListRV.layoutManager = layoutManager
        mHomeAdapter = HomeAdapter(activity)
        homeListRV.adapter = mHomeAdapter
    }
}