package com.comeonnow.patient.app.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.adapter.NotificationAdapter

class NotificationFragment : BaseFragment() {

    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.notificationListRV)
    lateinit var notificationListRV: RecyclerView

    @BindView(R.id.txtNoDataFoundTV)
    lateinit var txtNoDataFoundTV: TextView

    // - - Initialize Objects
    var mNotificationAdapter: NotificationAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_notification, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        setAdapter()
        return view
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        notificationListRV.layoutManager = layoutManager
        mNotificationAdapter = NotificationAdapter(activity)
        notificationListRV.adapter = mNotificationAdapter
    }
}