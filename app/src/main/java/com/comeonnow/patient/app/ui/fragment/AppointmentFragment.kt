package com.comeonnow.patient.app.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.adapter.AppointmentPagerAdapter
import com.comeonnow.patient.app.ui.activity.AddQueuedActivity
import com.google.android.material.tabs.TabLayout

class AppointmentFragment : BaseFragment() {
    // - - Initialize Widgets

    @BindView(R.id.addQueuedLL)
    lateinit var addQueuedLL: LinearLayout

    // - - Initialize Objects
    lateinit var mUnbinder: Unbinder
    private lateinit var pTabs: TabLayout
    private lateinit var pViewPager: ViewPager
    private lateinit var pagerAdapters: AppointmentPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_appointment, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        this.setViewPager(view)
        return view
    }

    @OnClick(
        R.id.addQueuedLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.addQueuedLL -> performAddQueuedClick()
        }
    }

    private fun performAddQueuedClick() {
        val intent = Intent(activity, AddQueuedActivity::class.java)
        startActivity(intent)
    }

    private fun setViewPager(view: View) {
        pViewPager = view.findViewById(R.id.viewPager)
        pTabs = view.findViewById(R.id.tabLayout)
        pagerAdapters = AppointmentPagerAdapter(childFragmentManager)
        // - - Setup Fragments List
        pagerAdapters.addFragment1(AllFragment(), "ALL")
        pagerAdapters.addFragment2(ConfirmedFragment(), "CONFIRMED")
        pagerAdapters.addFragment3(PendingFragment(), "PENDING")
        pagerAdapters.addFragment4(QueuedFragment(), "QUEUED")
        // - - Set ViewPager Adapter
        pViewPager.adapter = pagerAdapters
        // - - Set Tabs
        pTabs.setupWithViewPager(pViewPager)
    }

}

