package com.comeonnow.patient.app.ui.activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Build.VERSION_CODES.Q
import android.os.Bundle
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.util.*
import java.util.*
import java.util.regex.Pattern

open class BaseActivity : AppCompatActivity() {

    // - - Get Class Name
    var TAG = this@BaseActivity.javaClass.simpleName

    // - - Initialize Activity
    var mActivity: Activity = this@BaseActivity

    // - - Initialize other class objects
    var progressDialog: Dialog? = null

    //  Finishes Activity
    override fun finish() {
        super.finish()
        overridePendingTransitionSlideDownExit()
    }

    //  Start Activity
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransitionSlideUPEnter()
    }

    //  Overrides the pending Activity transition by performing the "Bottom Up" animation.
    fun overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0)
    }

    //  Overrides the pending Activity transition by performing the "Bottom Down" animation.
    fun overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down)
    }

    // - - To Check Whether User is logged_in or not
    fun isLogin(): Boolean {
        return AppPreference().readBoolean(mActivity, IS_LOGIN, false)
    }

    // - - To Get UserID
    fun getLoggedInUserID(): String {
        return AppPreference().readString(mActivity, USERID, "")!!
    }

    // - - To Get Auth_Token
    fun getAuthToken(): String {
        return AppPreference().readString(mActivity, AUTH_TOKEN, "")!!
    }

    // - - To Get Email
    fun getUserEmail(): String {
        return AppPreference().readString(mActivity, EMAIL, "")!!
    }

    // - - To Get Bio
    fun getUserBio(): String {
        return AppPreference().readString(mActivity, BIO, "")!!
    }

    // - - To Get PhoneNumber
    fun getUserPhoneNumber(): String {
        return AppPreference().readString(mActivity, PHONE_NUMBER, "")!!
    }

    // - - To Get Profile_Pic
    fun getUserProfilePic(): String {
        return AppPreference().readString(mActivity, PROFILE_PIC, "")!!
    }

    // - - To Check Internet Connection
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Q) {
            val connection =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (connection != null) {
                when {
                    connection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    connection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    connection.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) return true
        }
        return false
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Validate Email
    fun isValidEmailId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    // - - To Clear Focus From EditText
    fun setEditTextFocused(mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm =
                    v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    // - - Switch Between Fragments
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }
}




