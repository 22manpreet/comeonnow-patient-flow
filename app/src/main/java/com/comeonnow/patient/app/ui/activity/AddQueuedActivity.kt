package com.comeonnow.patient.app.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R
import kotlinx.android.synthetic.main.activity_add_queued.*

class AddQueuedActivity : BaseActivity() {

    // - - Initialize Objects
    private val data = arrayOf("Lorem", "Ipsum", "Valhalla")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_queued)
        ButterKnife.bind(this)
        hospitalSpinner()
        doctorSpinner()
        diseaseSpinner()
    }

    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    // - - Hospital Spinner
    private fun hospitalSpinner() {
        val adapter = ArrayAdapter(this, R.layout.spinner_item_selected, data)
        hospitalSpinner.adapter = adapter
        hospitalSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    // - - Doctor Spinner
    private fun doctorSpinner() {
        val adapter = ArrayAdapter(this, R.layout.spinner_item_selected, data)
        doctorSpinner.adapter = adapter
        doctorSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    // - - Disease Spinner
    private fun diseaseSpinner() {
        val adapter = ArrayAdapter(this, R.layout.spinner_item_selected, data)
        diseaseSpinner.adapter = adapter
        diseaseSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }
}