package com.comeonnow.patient.app.ui.activity

import android.content.Intent
import android.os.Bundle
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.util.SPLASH_TIME_OUT

class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                val i = Intent(mActivity, SignInActivity::class.java)
                startActivity(i)
                finish()
            }
        }
        mThread.start()
    }
}

