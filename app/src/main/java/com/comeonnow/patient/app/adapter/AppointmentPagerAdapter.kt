package com.comeonnow.patient.app.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.comeonnow.patient.app.ui.fragment.AllFragment
import com.comeonnow.patient.app.ui.fragment.ConfirmedFragment
import com.comeonnow.patient.app.ui.fragment.PendingFragment
import com.comeonnow.patient.app.ui.fragment.QueuedFragment


class AppointmentPagerAdapter(sFM: FragmentManager) :
    FragmentPagerAdapter(sFM, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    // - - Set fragment List
    private val pFragmentList = ArrayList<Fragment>()
    private val pFragmentTitle = ArrayList<String>()

    override fun getCount(): Int = pFragmentList.size

    override fun getItem(position: Int): Fragment = pFragmentList[position]

    override fun getPageTitle(position: Int): CharSequence = pFragmentTitle[position]

    fun addFragment1(fm: AllFragment, title: String) {
        pFragmentList.add(fm)
        pFragmentTitle.add(title)
    }

    fun addFragment2(fm: ConfirmedFragment, title: String) {
        pFragmentList.add(fm)
        pFragmentTitle.add(title)
    }

    fun addFragment3(fm: PendingFragment, title: String) {
        pFragmentList.add(fm)
        pFragmentTitle.add(title)
    }

    fun addFragment4(fm: QueuedFragment, title: String) {
        pFragmentList.add(fm)
        pFragmentTitle.add(title)
    }
}


