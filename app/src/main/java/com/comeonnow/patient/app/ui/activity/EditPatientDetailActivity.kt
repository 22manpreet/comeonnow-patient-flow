package com.comeonnow.patient.app.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isEmpty
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.comeonnow.patient.app.R
import com.github.dhaval2404.imagepicker.ImagePicker
import com.nekoloop.base64image.Base64Image
import com.nekoloop.base64image.RequestEncode
import kotlinx.android.synthetic.main.activity_add_patient.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

class EditPatientDetailActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.editNameET)
    lateinit var editNameET: EditText

    @BindView(R.id.birthdayTV)
    lateinit var birthdayTV: TextView

    // - - Initialize Objects
    private val data = arrayOf("Boy", "Girl", "Other")
    var mBitmap: Bitmap? = null
    var mBase64Image: String? = ""

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_patient_detail)
        ButterKnife.bind(this)
        setEditTextFocused(editNameET)
        genderSpinner()
    }

    @OnClick(
        R.id.backRL,
        R.id.birthdayTV,
        R.id.profilePicIV,
        R.id.btnSaveTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.birthdayTV -> showDatePicker(birthdayTV)
            R.id.profilePicIV -> performProfilePicClick()
            R.id.btnSaveTV -> performSaveClick()
        }
    }

    private fun performSaveClick() {
        if (isValidate()) {
            startActivity(Intent(mActivity, HomeActivity::class.java))
            finish()
        }
    }

    private fun performProfilePicClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(480)     //Final image size will be less than 480 MB (Optional)
            .maxResultSize(
                512,
                512
            )                          //Final image resolution will be less than 512 x 512 (Optional)
            .cropSquare()
            .start()
    }

    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_profile)
                        .into(profilePicIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage

                    Base64Image.with(this)
                        .encode(mBitmap)
                        .into(object : RequestEncode.Encode {
                            override fun onFailure() {
                                Log.e(TAG, "onFailure: ")
                                profilePicIV.setImageResource(0)
                            }

                            override fun onSuccess(p0: String?) {
                                Log.e(TAG, "onSuccess: ")
                                mBase64Image = p0!!
                            }
                        })

                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // - - Gender Spinner
    @SuppressLint("ClickableViewAccessibility")
    private fun genderSpinner() {
        // - - To Hide Keyboard
        genderSpinner.setOnTouchListener(View.OnTouchListener { v, event ->
            val imm =
                applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editNameET.windowToken, 0)
            false
        })
        // - - Set Adapter to spinner
        val adapter = ArrayAdapter(this, R.layout.spinner_item_selected, data)
        genderSpinner.adapter = adapter
        genderSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    // - - DatePicker
    private fun showDatePicker(mTextView: TextView) {
        // - - To Hide Keyboard
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(birthdayTV.windowToken, 0)
        // - - Initialize calender object
        val c = Calendar.getInstance()
        val mYear = c[Calendar.YEAR]
        val mMonth = c[Calendar.MONTH]
        val mDay = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(
            this,
            { view, year, monthOfYear, dayOfMonth ->
                var monthValue = ""
                monthValue = if (monthOfYear + 1 >= 10) {
                    (monthOfYear + 1).toString()
                } else {
                    "0" + (monthOfYear + 1).toString()
                }
                var dateValue = ""
                dateValue = if (dayOfMonth >= 10) {
                    dayOfMonth.toString()
                } else {
                    "0$dayOfMonth"
                }
                //dd-mm-yyyy
                val mActualDate: String = "$dateValue-$monthValue-$year"
                mTextView.text = mActualDate
            }, mYear, mMonth, mDay
        )
        val currentDate = System.currentTimeMillis()
        datePickerDialog.datePicker.maxDate = currentDate
        datePickerDialog.show()
    }

    // - - Validations for Edit Patient details fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            mBitmap == null -> {
                showAlertDialog(mActivity, getString(R.string.please_upload_image))
                flag = false
            }
            editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_name))
                flag = false
            }
            birthdayTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_dob))
                flag = false
            }
            genderSpinner.isEmpty() -> {
                showAlertDialog(mActivity, getString(R.string.please_select_gender))
                flag = false
            }
        }
        return flag
    }
}