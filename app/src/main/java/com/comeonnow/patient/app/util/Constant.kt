package com.comeonnow.patient.app.util

// - - Base Server Url
const val BASE_URL = "https://www.dharmani.com/ComeOnNow/"

// - - Constant Values
const val SPLASH_TIME_OUT = 1500
const val ACCEPT = 1
const val REJECT = 2

// - - Link Constants
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

// - - Link Urls
const val ABOUT_WEB_LINK = BASE_URL + "webservice/about.html"
const val PP_WEB_LINK = BASE_URL + "webservice/PrivacyAndPolicy.html"
const val TERMS_WEB_LINK = BASE_URL + "webservice/terms&services.html"

// - - Fragment Tags
const val HOME_TAG = "home_tag"
const val APPOINTMENT_TAG = "appointment_tag"
const val NOTIFICATION_TAG = "notification_tag"
const val PROFILE_TAG = "profile_tag"

// - - Shared Preference Keys
const val IS_LOGIN = "is_login"
const val USERID = "userID"
const val USERNAME = "user-name"
const val EMAIL = "email"
const val AUTH_TOKEN = "authToken"
const val BIO = "bio"
const val PHONE_NUMBER = "phone_number"
const val PROFILE_PIC = "profileImage"