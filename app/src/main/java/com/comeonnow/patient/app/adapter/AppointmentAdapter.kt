package com.comeonnow.patient.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.ui.activity.AppointmentDetailActivity


class AppointmentAdapter(var mActivity: Activity?) :
    RecyclerView.Adapter<AppointmentAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_all_appointment_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            mActivity?.startActivity(Intent(mActivity, AppointmentDetailActivity::class.java))
        }
    }

    override fun getItemCount(): Int {
        return 5
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var allListImage: ImageView = itemView.findViewById(R.id.allListIV)
        var allListNameTV: TextView = itemView.findViewById(R.id.allListNameTV)
        var allListAgeTV: TextView = itemView.findViewById(R.id.allListAgeTV)
        var allListGenderTV: TextView = itemView.findViewById(R.id.allListGenderTV)
        var allListDateTV: TextView = itemView.findViewById(R.id.allListDateTV)
        var allListTimeTV: TextView = itemView.findViewById(R.id.allListTimeTV)
        var allListIV1: ImageView = itemView.findViewById(R.id.allListIV1)
    }
}
