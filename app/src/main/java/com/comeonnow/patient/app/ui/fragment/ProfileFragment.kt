package com.comeonnow.patient.app.ui.fragment

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.core.app.ActivityCompat
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.ui.activity.ChangePasswordActivity
import com.comeonnow.patient.app.ui.activity.EditProfileActivity
import com.comeonnow.patient.app.ui.activity.SignInActivity

class ProfileFragment : BaseFragment() {

    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        return view
    }

    @OnClick(
        R.id.editRL,
        R.id.changePwdRL,
        R.id.logoutRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.editRL -> performEditProfileClick()
            R.id.changePwdRL -> performChangePwdClick()
            R.id.logoutRL -> performLogoutClick()
        }
    }

    private fun performEditProfileClick() {
        val intent = Intent(activity, EditProfileActivity::class.java)
        startActivity(intent)
    }

    private fun performChangePwdClick() {
        val intent = Intent(activity, ChangePasswordActivity::class.java)
        startActivity(intent)
    }

    private fun performLogoutClick() {
        showLogoutConfirmAlertDialog()
    }

    // - - Logout Alert Dialog
    private fun showLogoutConfirmAlertDialog() {
        val alertDialog = activity.let { Dialog(it!!) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()
            val intent = Intent(activity, SignInActivity::class.java)
            startActivity(intent)
            activity?.let { ActivityCompat.finishAffinity(it) }
        }
        alertDialog.show()
    }
}