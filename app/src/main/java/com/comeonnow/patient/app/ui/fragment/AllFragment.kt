package com.comeonnow.patient.app.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.adapter.AppointmentAdapter

class AllFragment : BaseFragment() {

    // - - Initialize Widgets
    @BindView(R.id.allAptListRV)
    lateinit var allAptListRV: RecyclerView

    // - - Initialize Objects
    lateinit var mUnbinder: Unbinder
    var mAllAppointmentAdapter: AppointmentAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_all, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        setAdapter()
        return view
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        allAptListRV.layoutManager = layoutManager
        mAllAppointmentAdapter = AppointmentAdapter(activity)
        allAptListRV.adapter = mAllAppointmentAdapter
    }
}