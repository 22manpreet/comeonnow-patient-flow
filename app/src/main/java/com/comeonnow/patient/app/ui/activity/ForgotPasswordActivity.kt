package com.comeonnow.patient.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R

class ForgotPasswordActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        ButterKnife.bind(this)
        setEditTextFocused(editEmailET)
    }

    @OnClick(
        R.id.backRL,
        R.id.btnSubmitTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.btnSubmitTV -> performSubmitClick()
        }
    }

    private fun performSubmitClick() {
        if (isValidate()) {
            startActivity(Intent(mActivity, SignInActivity::class.java))
            finish()
        }
    }

    // - - Validations for Forgot Password fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
        }
        return flag
    }
}