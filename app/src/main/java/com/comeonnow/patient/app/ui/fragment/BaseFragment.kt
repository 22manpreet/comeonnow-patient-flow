package com.comeonnow.patient.app.ui.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.util.*
import java.util.*
import java.util.regex.Pattern

open class BaseFragment : Fragment() {

    // - - Get Class Name
    var TAG = this@BaseFragment.javaClass.simpleName

    // - - Initialize other class objects
    var progressDialog: Dialog? = null

    // - - To Check Whether User is logged_in or not
    fun isLogin(): Boolean {
        return activity?.let { AppPreference().readBoolean(it, IS_LOGIN, false) }!!
    }

    // - - To Get UserID
    fun getLoggedInUserID(): String {
        return activity?.let { AppPreference().readString(it, USERID, "") }!!
    }

    // - - To Get Auth_Token
    fun getAuthToken(): String {
        return activity?.let { AppPreference().readString(it, AUTH_TOKEN, "") }!!
    }

    // - - To Get Email
    fun getUserEmail(): String {
        return activity?.let { AppPreference().readString(it, EMAIL, "") }!!
    }

    // - - To Get Bio
    fun getUserBio(): String {
        return activity?.let { AppPreference().readString(it, BIO, "") }!!
    }

    // - - To Get PhoneNumber
    fun getUserPhoneNumber(): String {
        return activity?.let { AppPreference().readString(it, PHONE_NUMBER, "") }!!
    }

    // - - To Get Profile_Pic
    fun getUserProfilePic(): String {
        return activity?.let { AppPreference().readString(it, PROFILE_PIC, "") }!!
    }

    // - - To Check Internet Connection
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Validate Email
    fun isValidEmailId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    // - - To Clear Focus From EditText
    fun setEditTextFocused(mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm =
                    v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }


}