package com.comeonnow.patient.app.ui.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.comeonnow.patient.app.R
import com.github.dhaval2404.imagepicker.ImagePicker
import com.nekoloop.base64image.Base64Image
import com.nekoloop.base64image.RequestEncode
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class EditProfileActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.editRelationshipET)
    lateinit var editRelationshipET: EditText

    // - - Initialize objects
    var mBitmap: Bitmap? = null
    var mBase64Image: String? = ""

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(this)
        setEditTextFocused(editRelationshipET)
    }

    @OnClick(
        R.id.backRL,
        R.id.profilePicIV,
        R.id.btnSaveTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.profilePicIV -> performProfilePicClick()
            R.id.btnSaveTV -> performSaveClick()
        }
    }

    private fun performSaveClick() {
        if (isValidate()) {
            onBackPressed()
        }
    }

    private fun performProfilePicClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(480)     //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                512,
                512
            )                          //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }

    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_profile)
                        .into(profilePicIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage

                    Base64Image.with(this)
                        .encode(mBitmap)
                        .into(object : RequestEncode.Encode {
                            override fun onFailure() {
                                Log.e(TAG, "onFailure: ")
                                profilePicIV.setImageResource(0)
                            }

                            override fun onSuccess(p0: String?) {
                                Log.e(TAG, "onSuccess: ")
                                mBase64Image = p0!!
                            }
                        })

                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // - - Validations for Edit Profile fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            mBitmap == null -> {
                showAlertDialog(mActivity, getString(R.string.please_upload_image))
                flag = false
            }
            editFirstNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_first_name))
                flag = false
            }
            editLastNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_last_name))
                flag = false
            }
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            editPhoneET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_phone_number))
                flag = false
            }
            editPhoneET.text.toString().length < 10 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_phone_number))
                flag = false
            }
            editRelationshipET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_relation))
                flag = false
            }
        }
        return flag
    }
}