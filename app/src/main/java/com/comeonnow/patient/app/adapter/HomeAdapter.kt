package com.comeonnow.patient.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.ui.activity.PatientDetailActivity


class HomeAdapter(var mActivity: Activity?) :
    RecyclerView.Adapter<HomeAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_home_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            mActivity?.startActivity(Intent(mActivity, PatientDetailActivity::class.java))
        }
    }

    override fun getItemCount(): Int {
        return 5
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var homeListImage: ImageView = itemView.findViewById(R.id.homeListIV)
        var homeListName: TextView = itemView.findViewById(R.id.homeListNameTV)
        var homeListAge: TextView = itemView.findViewById(R.id.homeListAgeTV)
        var homeListGender: TextView = itemView.findViewById(R.id.homeListGenderTV)
    }

}