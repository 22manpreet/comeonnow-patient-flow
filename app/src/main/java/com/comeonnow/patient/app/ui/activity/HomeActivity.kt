package com.comeonnow.patient.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R
import com.comeonnow.patient.app.ui.fragment.AppointmentFragment
import com.comeonnow.patient.app.ui.fragment.HomeFragment
import com.comeonnow.patient.app.ui.fragment.NotificationFragment
import com.comeonnow.patient.app.ui.fragment.ProfileFragment
import com.comeonnow.patient.app.util.APPOINTMENT_TAG
import com.comeonnow.patient.app.util.HOME_TAG
import com.comeonnow.patient.app.util.NOTIFICATION_TAG
import com.comeonnow.patient.app.util.PROFILE_TAG
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.actionHome)
    lateinit var actionHome: LinearLayout

    @BindView(R.id.actionAppointment)
    lateinit var actionAppointment: LinearLayout

    @BindView(R.id.actionNotification)
    lateinit var actionNotification: LinearLayout

    @BindView(R.id.actionProfile)
    lateinit var actionProfile: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        setUpFirstFragment()
    }

    @OnClick(
        R.id.actionHome,
        R.id.actionAppointment,
        R.id.actionNotification,
        R.id.actionProfile

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.actionHome -> performHomeClick()
            R.id.actionAppointment -> performAppointmentClick()
            R.id.actionNotification -> performNotificationClick()
            R.id.actionProfile -> performProfileClick()
        }
    }

    // - - SetUp HomeFragment on HomeScreen
    private fun setUpFirstFragment() {
        performHomeClick()
    }

    // - - Opens HomeFragment
    private fun performHomeClick() {
        tabSelection(0)
        switchFragment(HomeFragment(), HOME_TAG, false, null)
    }

    // - - Opens AppointmentFragment
    private fun performAppointmentClick() {
        tabSelection(1)
        switchFragment(AppointmentFragment(), APPOINTMENT_TAG, false, null)
    }

    // - - Opens NotificationFragment
    private fun performNotificationClick() {
        tabSelection(2)
        switchFragment(NotificationFragment(), NOTIFICATION_TAG, false, null)
    }

    // - - Opens ProfileFragment
    private fun performProfileClick() {
        tabSelection(3)
        switchFragment(fragment = ProfileFragment(), PROFILE_TAG, false, null)
    }

    // In your activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int) {
        imgHomeIV.setImageResource(R.drawable.ic_home)
        imgAppointmentIV.setImageResource(R.drawable.ic_appointment)
        imgNotificationIV.setImageResource(R.drawable.ic_notification)
        imgProfileIV.setImageResource(R.drawable.ic_profile)
        txtHomeTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextGrey))
        txtAppointmentTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextGrey))
        txtNotificationTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextGrey))
        txtProfileTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextGrey))
        when (mPos) {
            0 -> {
                imgHomeIV.setImageResource(R.drawable.ic_home_sel)
                txtHomeTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
            1 -> {
                imgAppointmentIV.setImageResource(R.drawable.ic_appointment_sel)
                txtAppointmentTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
            2 -> {
                imgNotificationIV.setImageResource(R.drawable.ic_notification_sel)
                txtNotificationTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
            3 -> {
                imgProfileIV.setImageResource(R.drawable.ic_profile_sel)
                txtProfileTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
        }

    }
}