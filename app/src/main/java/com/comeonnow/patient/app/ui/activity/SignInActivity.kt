package com.comeonnow.patient.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    // - - Initialize Objects
    private var showHide: Int = 1
    private var checkUncheck: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        ButterKnife.bind(this)
        setEditTextFocused(editPasswordET)
    }

    @OnClick(
        R.id.txtForgotPasswordTV,
        R.id.txtSignUpTV,
        R.id.btnLogInTV,
        R.id.imgShowHidePwdIV,
        R.id.rememberMeRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtForgotPasswordTV -> performForgotPasswordClick()
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.btnLogInTV -> performLogInClick()
            R.id.imgShowHidePwdIV -> performShowHideClick()
            R.id.rememberMeRL -> performRememberMeClick()
        }
    }

    private fun performForgotPasswordClick() {
        startActivity(Intent(mActivity, ForgotPasswordActivity::class.java))
    }

    private fun performSignUpClick() {
        startActivity(Intent(mActivity, SignUpActivity::class.java))
        finish()
    }

    private fun performLogInClick() {
        if (isValidate()) {
            startActivity(Intent(mActivity, HomeActivity::class.java))
            finish()
        }
    }

    // - - Perform Remember me
    private fun performRememberMeClick() {
        if (checkUncheck == 0) {
            imgRememberMeIV.setImageResource(R.drawable.ic_check)
            checkUncheck++
        } else {
            imgRememberMeIV.setImageResource(R.drawable.ic_uncheck)
            checkUncheck--
        }
    }

    // - - Show or Hide Password
    private fun performShowHideClick() {
        if (showHide == 0) {
            imgShowHidePwdIV.setImageResource(R.drawable.ic_hide_pwd)
            editPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
            editPasswordET.setSelection(editPasswordET.text.length)
            showHide++
        } else {
            imgShowHidePwdIV.setImageResource(R.drawable.ic_show_pwd)
            editPasswordET.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editPasswordET.setSelection(editPasswordET.text.length)
            showHide--
        }
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
        }
        return flag
    }
}