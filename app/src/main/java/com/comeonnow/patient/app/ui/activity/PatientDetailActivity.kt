package com.comeonnow.patient.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R

class PatientDetailActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_detail)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.backRL,
        R.id.editRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.editRL -> performEditClick()
        }
    }

    private fun performEditClick() {
        startActivity(Intent(mActivity, EditPatientDetailActivity::class.java))
    }
}