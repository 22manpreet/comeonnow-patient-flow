package com.comeonnow.patient.app.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.patient.app.R
import kotlinx.android.synthetic.main.activity_change_password.*

class ChangePasswordActivity : BaseActivity() {

    // - - Initialize Widgets
    @BindView(R.id.editConfirmNewPasswordET)
    lateinit var editConfirmNewPasswordET: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        ButterKnife.bind(this)
        setEditTextFocused(editConfirmNewPasswordET)
    }

    @OnClick(
        R.id.backRL,
        R.id.btnSaveTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.btnSaveTV -> performSaveClick()
        }
    }

    private fun performSaveClick() {
        if (isValidate()) {
            onBackPressed()
        }
    }

    // - - Validations for Change password fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editOldPasswordET!!.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_current_password))
                flag = false
            }
            editNewPasswordET!!.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
                flag = false
            }
            editNewPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            editConfirmNewPasswordET.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_retype_password))
                flag = false
            }
            editNewPasswordET!!.text.toString().trim() != editConfirmNewPasswordET.text.toString()
                .trim() -> {
                showAlertDialog(mActivity, getString(R.string.the_new_re_typed))
                flag = false
            }
        }
        return flag
    }
}