package com.comeonnow.patient.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.patient.app.R

class NotificationAdapter(
    var mActivity: Activity?
) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {

    // - - Initialize Objects
    var TAG: String = "NotificationsAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_notification_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var notificationListIV: ImageView = itemView.findViewById(R.id.notificationListIV)
        var notificationButtonLL: LinearLayout = itemView.findViewById(R.id.notificationButtonLL)
        var notificationListTV: TextView = itemView.findViewById(R.id.notificationListTV)
        var txtAcceptTV: TextView = itemView.findViewById(R.id.txtAcceptTV)
        var txtDeclineTV: TextView = itemView.findViewById(R.id.txtDeclineTV)
        var txtDescriptionTV: TextView = itemView.findViewById(R.id.txtDescriptionTV)
        var txtTimeTV: TextView = itemView.findViewById(R.id.txtTimeTV)
    }
}
